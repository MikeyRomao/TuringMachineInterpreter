package mromao_SE3310B_Assignment3;

import java.util.ArrayList;
import java.util.Scanner;

public class TuringMachineMain {
    
    public static void main(String[] args)
    {
        ArrayList<String> transitionArray = new ArrayList<String>();
        String tape = "";
        boolean isSkipped = false;
        char currentState = '0', direction= ' ', newLetter= ' ';
        int currentIndex = 0;
        String finishedStatus = "";
        ArrayList<String> acceptingStates = new ArrayList<String>();
        Scanner newFile = null;
        newFile = new Scanner(System.in); //scanning a new text file
        
        //Parsing the input
        while(newFile.hasNextLine())
        {
            String myNextLine = newFile.nextLine();
            if(myNextLine.charAt(0) == 'i' && myNextLine.charAt(1) == ' ') //if the line contains an i as the first letter and then a space, we know it's the tape
            {
                myNextLine.trim();
                tape = myNextLine.substring(2,myNextLine.length()); //get the tape as a string
                
            }
            
            else if(myNextLine.charAt(0) == 't' && myNextLine.charAt(1) == ' ') //if the line contains a t as the first letter and then a space, we know it's a transition function
            {
                myNextLine = myNextLine.replace('t', ' '); //remove t
                myNextLine = myNextLine.substring(2,myNextLine.length()); //shift entire string left
                myNextLine.trim();
                transitionArray.add(myNextLine);
            }
            
            else if(myNextLine.charAt(0) == 'f' && myNextLine.charAt(1) == ' ') //if the line contains an f as the first letter and then a space, we know it's a set of final states
            {
                String [] myString;
                myNextLine.trim();
                myString = myNextLine.split(" "); //split the array on every space
                for(int j=0; j<myString.length; j++)
                {
                    acceptingStates.add(myString[j]); //if the array element does not equal f, then add it to the array of accepting states
                }
                acceptingStates.remove("f"); //remove the f from the array
            }
        }
        
        boolean isFinished=false;
        
        while(!isFinished) //iterate through
        {
            while(!isSkipped)
            {
                char [] myTape=tape.toCharArray();
                if(myTape[currentIndex] == '_') //if the tape starts with '_', move the head right
                {
                    currentIndex++;
                }
                
                else
                {
                    isSkipped = true; //if the currentIndex is not '_', the head is in the right spot
                }
            }
            
            for(int i=0; i<transitionArray.size(); i++)
            {
                if(transitionArray.get(i).charAt(0) == currentState && tape.charAt(currentIndex) == transitionArray.get(i).charAt(2))
                {
                    currentState = transitionArray.get(i).charAt(4); //switch states
                    newLetter = transitionArray.get(i).charAt(6);
                    direction = transitionArray.get(i).charAt(8);
                    
                    if(direction == 'L' || direction == 'l')
                    {
                        char [] myTape = tape.toCharArray(); //strings are immutable in java, therefore must use char array to replace instance
                        myTape[currentIndex] = newLetter;
                        tape = String.valueOf(myTape);
                        currentIndex--; //move Left
                        i = -1; //restart the loop in case there are transition functions needed   
                    }
                    
                    else
                    {
                        char [] myTape=tape.toCharArray(); //strings are immutable in java, therefore must use char array to replace instance
                        myTape[currentIndex] = newLetter;
                        tape = String.valueOf(myTape);
                        currentIndex++; //move Right
                        i = -1; //restart the loop in case there are transition functions needed
                    }   
                }
                
                else if(i == transitionArray.size() - 1); //if the for loop is done and found variable is true
                {
                    for(int j = 0; j<acceptingStates.size(); j++)
                    {
                        if(acceptingStates.get(j).charAt(0) == currentState) //if the currentState is an accepting state
                        {
                            finishedStatus = "ACCEPT"; //make the status accept
                            isFinished = true;
                            break;
                        }
                        
                        else if(j == acceptingStates.size()-1)
                        {
                            finishedStatus = "REJECT"; //make the status reject
                            isFinished = true;
                        }
                    }
                }
            } 
        }
        
        System.out.println(tape);
        System.out.println(finishedStatus);  
    }
}